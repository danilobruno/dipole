function [r,out] = viapointsVecReward(P,pars)
	%%Reward for viapoint task
	%pars: additional parameters for task
	
	%pars.Target:		viapoints
	%pars.Sigma:		sigma of DS-GMM
	%pars.InitialPoint  Initial Point
	
	sys = pars.sys;
	Target = pars.Target;

	%Target = [1 2 3 4 5 6;3 2 1 0 2 6];
	TargetPoint = Target([2:end],end);
	%Alpha = sys.alpha;
	r =0;
	Alpha = 0.5;
	%Alpha = 1;
	nbStates = size(pars.Sigma,3);
	nbVars = size(pars.InitialPoint,1);
	nbVar = size(pars.InitialPoint,1);
	Mu = reshape(P,nbVars+1,nbStates);

	results = dmpSprings(Mu,pars.Sigma,pars.InitialPoint,sys);
	%results = dmpSpringsT(Mu,pars.Sigma,pars.InitialPoint,pars.Target(:,end),sys);

 	%for i=1:size(Target,2)
 	%	for k=1:size(results.Data,2)
	%		dist(k) = exp(-Alpha*((Target(:,i)-results.Data([1:2],k))'*(Target(:,i)-results.Data([1:2],k))));
 	%	end
 	%	r = r + min(dist);
 	%end
	%r = r/size(Target,2);
 	nbData = size(results.Data,2);
	index = zeros(1,size(Target,2));
 	%index = linspace(1,nbData,size(Target,2)+1);
	index = nbData*Target(1,:);
 	index = round(index);
 	for i=1:size(Target,2)
		dist(i) = exp(-Alpha*((Target([2:end],i)-results.Data([1:nbVar],index(i)))'*(Target([2:end],i)-results.Data([1:nbVar],index(i)))));
	end
	times = unique(Target(1,:));
	for i = 1:size(times,2)
		VV = Target(1,:) == times(i);
		dd(i) = max(dist(VV));
	end
	r = dd';
	out.Target = Target;
	out.results = results;
end

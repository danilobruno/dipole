function results =  rldp(mu0,sigma0,nbEpisodes,nbInitialPoints,rewardFunction,choiceAlg,choiceStart,Min,Max,pars)


%RLDP Implements a research of multioptimal policy starting from given 
%				parameters and with a given starting exploration noise
%
%Notes:
%	Uses Dirichlet processes with Neal algorithm 8
%
%	Reward is treated as an additional dimension for clustering
%	
%	Samples new exploration points using DP

%Author:		Danilo Bruno
%Date:			September, 2012
%
%INPUT:
%
%	choiceAlg 		1:	POWER	2: 	CEM
%	choiceStart		1:	Local	2: 	Global
%
%OUTPUT:
%
%	structure with results
%
%% Algorithm:
%
%	- nbE points are sampled around the initial point with the given exploration noise
%
%	- Points and rewards for points are put together into a unique vector, on which clustering is performed
%	
%	for episode = 1:nbEpidodes
%
%		- DP clustering is performed of the points, determining K(episode) policies
%
%		- 10 POWER steps are performed 
%			- at each step a new point is sampled from each policy and added to the previous ones
%			- only the best nP points are kept for each cluster at each step
%			- POWER policy update is performed
%
%	end
%	
%%%%%
%
%N.B. 
%- The total number of RL episodes in 10*nbEpisodes
%- The total number of evaluated rewards is nP + sum_{episode=1}^nbE 10*K(episode) 

%% Parameters
%

disp('DIPOLE runninG....');
%Space parameters
nbVar = size(sigma0,1);

totEvalRewards = 0;


%RL Parameters
nbE = nbInitialPoints;			%Number of points to sample at the beginning
nbPointsRegr = nbE;	
%DP Parameters
alpha = 1;		%Alpha parameter of DP
nbIters = 100;	%Iterations of DP
m = 3;			%Number of new clusters
%Priors for DP
r0 = rewardEval(mu0,str2func(rewardFunction),pars);	%Evaluate reward of starting point for later DP evaluation
Sigmastart = sigma0;
muStart = mu0;
lambda0 = sigma0;
nu0 = nbVar +3;
k0 = 1;
PriorReward = 0.5;
%Number of total variables (with added reward)
nbVarAll = nbVar +1;


%% Algorithm
%
%
tic

if choiceStart == 1
	%Sample nbE points from the first exploration gaussian
	P = mvnrnd(mu0,lambda0,nbE)';	%Sample from normal
	r = zeros(1,nbE);
	for i=1:nbE
		r(i) = rewardEval(P(:,i),str2func(rewardFunction),pars);				%Evaluate reward
		totEvalRewards = totEvalRewards + 1;
	end
elseif choiceStart == 2
	%Sample random points from the whole space (at least 20)
	P = repmat(Min,1,nbE) + repmat((Max - Min),1,nbE) .* rand(nbVar,nbE);	%Ramdom policy
	r = zeros(1,nbE);
	for i=1:nbE
		r(i) = rewardEval(P(:,i),str2func(rewardFunction),pars);							%Evaluate reward
		totEvalRewards = totEvalRewards + 1;
	end
end


%Put data and reward into a unique vector

y = [P;r];
yAll = y;		%For plotting

%Set initial values for DP (1 cluster for all)
K = 1;					%Total number of clusters
MU = mean(y')';
SIGMA = cov(y');
%convFact = 0;
NUM = nbE;
c= ones(1,nbE); 
%Keep data
results(1).Mu = MU;
results(1).Sigma = SIGMA;
results(1).totEvalRewards = totEvalRewards;
index = 1;
minsigma = SIGMA/100000;
SIGMA = SIGMA + 0.0000001*eye(nbVarAll);

N = nbE;				%Total number of points to cluster
nP = 10;
nbE = 1;				%New points to sample from now on
%RL episodes
for episode = 1:nbEpisodes
%	Cluster with DP
%
	for iter = 1:nbIters 
		
		%step 1: iteration over all the elements
		for i=1:N
			 y_ = y(:,i);
			%if c(i) is a singleton
			 if NUM(c(i)) == 1
				 %number of cluster values different from c(i)
				 K_ = K - 1;
				 %put the singleton cluster in the last position
				 oldind = c(i);
				 mutmp = MU(:,oldind);
				 sigmatmp = SIGMA(:,:,oldind);
				 NUM(oldind) = NUM(K);
				 MU(:,oldind) = MU(:,K);
				 SIGMA(:,:,oldind) = SIGMA(:,:,K);
				 MU(:,K) = mutmp;
				 SIGMA(:,:,K) = sigmatmp;
				 NUM(K) = 1;
				 c(c==K)= oldind;
				 c(i) = K;             
			 else
				 %number of cluster values different from c(i)
				 K_ = K;                 
			 end      
			 %total clusters to produce
			 h = K_ + m;
			 %sample parameters for the auxiliary clusters
			 for jj=K+1:h
				 %cl(jj).mu = mvnrnd(mu0,sigma0);
				 %muP = Min + (Max - Min) .* rand(2,1); %mu sampled from the uniform
				 [muP sigmaP] = PriorSampleNormIWish(mu0,k0,lambda0,nu0);
				 MU(:,jj) = [muP';rand(1,1)];
				 SIGMA(:,:,jj) = [sigmaP zeros(nbVar,1) ; zeros(1,nbVar) PriorReward];
				 NUM(jj) = 0;	%auxililary clusters are empty
			 end
			 %resample c(i) with probability of dp
			 %%%build probabilities
			 %%determine probabilities for the first k_ clusters
			 p = zeros(1,h);
			 for k=1:K_
				 p(k) = NUM(k);
				 if k==c(i)
					 p(k) = p(k)-1;
				 end
				 p(k) = MU(end,k)*p(k)/(N-1+alpha) * mvnpdf(y_,MU(:,k),SIGMA(:,:,k));
			 end
			 %determine probabilities for the auxiliary clusters
			 for k=K_+1:h
				 p(k) = MU(end,k)*(alpha/m)/(N-1+alpha)*mvnpdf(y_,MU(:,k),SIGMA(:,:,k));
			 end
			 %%%sample cluster for ith data
			 a = catsample(p);
			 %move point to the new cluster
			 NUM(c(i)) = NUM(c(i)) -1;%eliminate from old
			 NUM(a) = NUM(a) + 1;%put into new
			 c(i) = a; %reassign label
			 
			%sweep clusters to eliminate empty ones
			%put the non empty clusters in a new structure cl_
			%put the new cluster labels in a new variable c_ 
			%new number of clusters
			sel = NUM == 0; %The zeros are the non empty clusters
			idx = find(NUM);%Indexes of non empty clusters 
			MU = MU(:,sel==0);
			SIGMA = SIGMA(:,:,sel==0);
			NUM = NUM(sel==0);
			K = length(idx);
			for k=1:K
				c(c==idx(k)) = k;
			end
		end
		%step 2: draw new values for the parameters of each cluster
		for k=1:K
			ycl = y(:,c==k); %data belonging to cluster
			cn = NUM(k);    %number of objects in cluster
			%new experimental mean
			xx = zeros(nbVarAll,1);
			for jj=1:cn
				xx = xx + ycl(:,jj);
			end
			xx = xx/cn;
			%new experimental covariance
			S = zeros(nbVarAll);
			for j=1:cn
				S = S + (ycl(:,j) - xx)*(ycl(:,j) - xx)';
			end
			%posterior parameters for iwish
			lambda0_ = [lambda0 zeros(nbVar,1) ; zeros(1,nbVar) PriorReward] + S + ... 
				(k0*cn)/(k0+cn)*(xx - [mu0;r0])*(xx-[mu0;r0])';
			nu0_ = nu0 + cn;
			SIGMA(:,:,k) = iwishrnd(lambda0_,nu0_); 
			MU(:,k) = xx;
			%sigma0_ = inv(inv(sigma0)+cn*inv(cl(k).sigma));
			%mu0_ = sigma0_*(cn*inv(cl(k).sigma)*xx' + inv(sigma0)*mu0');
			%cl(k).mu = mvnrnd(mu0_,sigma0_);
			%cl(k).mu = xx;
		end
	end
	nbIters = 30;	%Different only for the first iteration

	for rlstep=1:10
%	Sample 1  point for each cluster at each iteration 
	PNoisy = zeros(nbVar,K);
	cNoisy = zeros(1,K);
	if choiceAlg == 4
		PNoisy = [];
		cNoisy = [];
		rNoisy = [];
	end
	for k=1:K
		if (choiceAlg == 1) | (choiceAlg == 2)
			%PNoisy(:,k) = mvnrnd(MU([1:nbVar],k),SIGMA([1:nbVar],[1:nbVar],k));
			PNoisy(:,k) = MU([1:nbVar],k)+SIGMA([1:nbVar],[1:nbVar],k)*randn(nbVar,1);
			cNoisy(k) = k;
			NUM(k) = NUM(k) +1;
		elseif (choiceAlg == 3)
			PNoisy(:,k) = MU([1:nbVar],k);
			cNoisy(k) = k;
			NUM(k) = NUM(k) +1;
		elseif (choiceAlg == 4)
			%Sample points from new
			for nnn=1:5
				PTmp(:,nnn) = mvnrnd(MU([1:nbVar],k),SIGMA([1:nbVar],[1:nbVar],k))';
				cTmp = k*ones(1,5);
				NUM(k) = NUM(k) + 1;
				rTmp(nnn) = rewardEval(PTmp(:,nnn),str2func(rewardFunction),pars);
			end
			PNoisy = [PNoisy PTmp];
			cNoisy = [cNoisy cTmp];
			rNoisy = [rNoisy rTmp];
			yNoisy = [PNoisy;rNoisy];
			MU(:,k) = mean(yNoisy')';
			SIGMA(:,:,k) = cov(yNoisy') + 0.01*eye(nbVarAll);
		end
	end
	%end
	%Add to previous P
	if (choiceAlg == 4)
		P = [PNoisy];
		r = [rNoisy];
		c = [cNoisy];
	else
		rNoisy = zeros(1,size(PNoisy,2));
		for i=1:size(PNoisy,2)
			rNoisy(i) = rewardEval(PNoisy(:,i),str2func(rewardFunction),pars);
			totEvalRewards = totEvalRewards + 1;
		end
		P = [P PNoisy];
		r = [r rNoisy];
		c = [c cNoisy];
	end

	y = [P;r];		%Put parameters and rewards into a unique vector

	yNoisy = [PNoisy;rNoisy];
	yAll = [yAll yNoisy];
	
	N=0;
	ynew=[];
	cnew=[];
	if choiceAlg == 1
	%Update cluster parameters with POWER 
		for k=1:K
			ycl = y(:,c==k); %data belonging to cluster
			cn = NUM(k);    %number of objects in cluster
			rcl = ycl(end,:);
			[rSrt,idSrt] = sort(rcl,'descend');


			%Update with number
			nbP = min(length(idSrt),nP);
			yclTmp = ycl(:,idSrt(1:nbP));
			rTmp = rSrt(1:nbP);
			%Compute error term
			eTmp = yclTmp - repmat(MU(:,k),1,nbP);

			%Update with threshold
			%TreshHold = rSrt(1)*0.95; 
			%BestIdx = ge(rcl,TreshHold);
			%yclTmp = ycl(:,BestIdx==1);
			%nP = sum(BestIdx);
			%if nP > 2 
			%	cn = nP;
			%	NUM(k) = nP;
			%	N = N + nP;
			%	nbP = nP;
			%	ynew = [ynew yclTmp];
			%	cnew = [cnew k*ones(1,nbP)];
			%	rTmp = rSrt(1:nbP);
			%	%%Compute error term
			%	eTmp = yclTmp - repmat(MU(:,k),1,nbP);
			%else
			%	%MU(:,k) = MU(:,k);
			%	%SIGMA(:,:,k) = SIGMA(:,:,k);
			%	ynew = [ynew ycl];
			%	cnew = [cnew k*ones(1,cn)];
			%	NUM(k) = cn;
			%	N = N+cn;
			%	nbP = cn;
			%	rTmp = rSrt(1:nbP);
			%	%%Compute error term
			%	eTmp = ycl - repmat(MU(:,k),1,nbP);
			%end

			%Standard udpate of the policy
			%
			%Update the parameters of each cluster with POWER
			if sum(rTmp) > 0
				MU(:,k) = MU(:,k) + eTmp*rTmp'/sum(rTmp);
				%SIGMA(:,:,k) = (eTmp*diag(rTmp)*eTmp')/sum(rTmp) + minsigma;
			else
				MU(:,k) = MU(:,k);
				SIGMA(:,:,k) = SIGMA(:,:,k) + minsigma;
			end

			%MU(:,k) = mean(yclTmp')';
			%SIGMA(:,:,k) = cov(yclTmp') + 0.01*eye(nbVarAll);


			%Only keep points with higher reward
			ynew = [ynew yclTmp];
			cnew = [cnew k*ones(1,nbP)];
			NUM(k) = nbP;
			N = N+nbP;
		end
	elseif choiceAlg == 2
		%Update cluster parameters with CEM
		for k=1:K
			ycl = y(:,c==k); %data belonging to cluster
			cn = NUM(k);    %number of objects in cluster
			rcl = ycl(end,:);
			[rSrt,idSrt] = sort(rcl,'descend');
			%TreshHold = rSrt(end) + (rSrt(1) - rSrt(end))*0.5;
			TreshHold = rSrt(1)*0.95; 
			BestIdx = ge(rcl,TreshHold);
			yclTmp = ycl(:,BestIdx==1);
			nP = sum(BestIdx);
			%Standard udpate of the policy
			%
			if nP > 2
				cn = nP;
				MU(:,k) = mean(yclTmp')';
				SIGMA(:,:,k) = cov(yclTmp') + 0.01*eye(nbVarAll);
				ynew = [ynew yclTmp];
				cnew = [cnew k*ones(1,cn)];
				NUM(k) = cn;
				N = N+cn;
			else
				MU(:,k) = MU(:,k);
				SIGMA(:,:,k) = SIGMA(:,:,k);
				ynew = [ynew ycl];
				cnew = [cnew k*ones(1,cn)];
				NUM(k) = cn;
				N = N+cn;
			end
		end
	elseif choiceAlg == 3 
		for k=1:K
			ycl = y(:,c==k); %data belonging to cluster
			cn = NUM(k);    %number of objects in cluster
			rcl = ycl(end,:);
			[rSrt,idSrt] = sort(rcl,'descend');
			nbP = min(length(idSrt),nP);
			yclTmp = ycl(:,idSrt(1:nbP));
			rTmp = rSrt(1:nbP);
			%eTmp = yclTmp - repmat(MU(:,k),1,nbP);
			%Standard udpate of the policy
			%
			%Update the parameters of each cluster with POWER
			%if sum(rTmp) > 0
			%	MU(:,k) = MU(:,k) + eTmp*rTmp'/sum(rTmp);
				%SIGMA(:,:,k) = (eTmp*diag(rTmp)*eTmp')/sum(rTmp) + minsigma;
			%else
			%	MU(:,k) = MU(:,k);
			%	SIGMA(:,:,k) = SIGMA(:,:,k) + minsigma;
			%end
			%Only keep points with higher reward
			ynew = [ynew yclTmp];
			cnew = [cnew k*ones(1,nbP)];
			NUM(k) = nbP;
			N = N+nbP;
			%Update policy with GMR
			MU([1:end-1],k) = MU([1:end-1],k) + SIGMA([1:end-1],end,k) * inv(SIGMA(end,end,k)) * (0.70 - MU(end,k));
			%MU(end,k) = mean(yclTmp(end,:));
			%SIGMA([1:end-1],[1:end-1],k) = SIGMA([1:end-1],[1:end-1],k) - SIGMA([1:end-1],end,k)*inv(SIGMA(end,end,k))*SIGMA(end,[1:end-1],k) + 0.01*eye(nbVar);
			SIGMA(:,:,k) = cov(yclTmp') + 0.01*eye(nbVarAll);
		end
	elseif (choiceAlg == 4)
		for k=1:K
			ycl = y(:,c==k); %data belonging to cluster
			cn = NUM(k);    %number of objects in cluster
			rcl = ycl(end,:);
			[rSrt,idSrt] = sort(rcl,'descend');


			%Update with number
			nbP = min(length(idSrt),nP);
			yclTmp = ycl(:,idSrt(1:nbP));
			rTmp = rSrt(1:nbP);
			NUM(k) = nbP;
			N = N+nbP;
			ynew = [ynew yclTmp];
			cnew = [cnew k*ones(1,nbP)];
			MU([1:end-1],k) = MU([1:end-1],k) + SIGMA([1:end-1],end,k) * inv(SIGMA(end,end,k)) * (rSrt(1) - MU(end,k));
			SIGMA([1:end-1],[1:end-1],k) = SIGMA([1:end-1],[1:end-1],k) - SIGMA([1:end-1],end,k)*inv(SIGMA(end,end,k))*SIGMA(end,[1:end-1],k); 
			

		end
	end
		y = ynew;
		c = cnew;
		P = y(1:nbVar,:);
		r = y(end,:);
		index = index +1;
		results(index).Mu = MU;
		results(index).Sigma = SIGMA;
		pr = zeros(1,size(SIGMA,3));
		for k=1:size(SIGMA,3)
			aa(k) = MU(end,k) + (1-MU(end,k))*MU(end,k)^2/SIGMA(end,end,k);		%Parameters of beta distribution corresponding to r dist
			bb(k) = (1 - MU(end,k)) + (1-MU(end,k))^2*MU(end,k)/SIGMA(end,end,k);
			pr(k) = 1 - betacdf(0.95,aa(k),bb(k)); %Evaluate probability to get a reward in the last 20ile
		end
		results(index).probRew = pr;
		results(index).totEvalRewards = totEvalRewards;

%		for k=1:K
%			if MU(end,k) == 1001
%				MU = MU(:,k);
%				SIGMA = 0.1 * eye(nbVarAll);
%				K = 1;
%				y = y(:,c==k);
%				N = NUM(k);
%				NUM = N;
%				break;
%			end
%		end


		fprintf('Episode : %d ; Number of optimal policies: %d \n',index,K);
		fprintf('Rew:\t ');
		for k=1:K
			fprintf('%d\t',MU(end,k));
		end
		fprintf('\n');
		%fprintf('Convergence probability :\t ');
		%for k=1:K
		%	fprintf('%d\t',convFact(k) > 0.01 );
		%end
		%fprintf('\n');
		fprintf('Prob:\t ');
		for k=1:K
			fprintf('%d\t',pr(k));
		end
		fprintf('\n');
		fprintf('Value:\t ');
		for k=1:K
			fprintf('%d\t',pr(k)*MU(end,k));
		end
		fprintf('\n');
	end

	nP = 5;
	
	%Update priors
	%Calculate mean among clusters
	muBar = zeros(nbVar,1);
	sigmaBar = zeros(nbVar);
	for k=1:K
		muBar = muBar + MU([1:nbVar],k);
	end
	muBar = muBar/K;
	%Calculate covariance among clusters
	for k=1:K
		sigmaBar = sigmaBar + (MU([1:nbVar],k)-muBar)*(MU([1:nbVar],k)-muBar)';
	end
	%Update priors
	mu0_ = k0/(k0+K)* mu0 + K/(k0+K) * muBar;
	k0_ = k0 + K;
	nu0_ = nu0 + K;
	lambda0_ = lambda0 + sigmaBar + (k0*K)/(k0+K)* (muBar- mu0)*(muBar - mu0)';
	%[mu0Tmp lambda0] = PriorSampleNormIWish(mu0_,k0_,lambda0_,nu0_);
	mu0 = mu0_;
	%lambda0 = lambda0_;
	%nu0 = nu0_;
	%k0 = k0_;


end
%
%

results(index).totalTime = toc;
results(index).totEvalRewards = totEvalRewards;


end


%Sample from multinomial with probability vector p
function a = catsample(p)
% Samples from the multinomial with probabilities p
	z = rand(1,1);
	p = p/sum(p);
	t = cumsum(p);
	%a contains the cluster where the point fits
	a = sum(repmat(z,1,numel(p)) >= repmat(t,1,1),2)+1;
end
 

function [ mu,sigma ] = PriorSampleNormIWish( mu0,k0,lambda0,nu0 )
%PRIORSAMPLENORMIWISH samples mu and sigma from normal inverse wishart
%   
% needs: statistics toolbox
%
% Author: Danilo Bruno
% Date: May 2012

sigma = iwishrnd((lambda0),nu0);
mu = mvnrnd(mu0,sigma/k0);


end


function r = rewardEval(p,reward,pars)
	
		r = reward(p,pars);

end
%function r = rewardEval(p)
%Mu = zeros(2,3);
%Sigma = zeros(2,2,3);
%i=0;
%Mu(:,1)=[3;3]; Mu(:,2)=[7;7]; Mu(:,3)=[1.5;8.5];
%Sigma(:,:,1)=diag([1,9]); Sigma(:,:,2)=diag([9,1]); Sigma(:,:,3)=diag([.5,.5]);
%Priors = [1 1 0.6];
%Priors = Priors/sum(Priors);
%r = zeros(size(p,2),1);
%r_max = 0;
%for i=1:size(Mu,2)
%  r = r + Priors(i) * gaussPDF(p,Mu(:,i),Sigma(:,:,i));
%  r_max = r_max + Priors(i) * gaussPDF(Mu(:,i),Mu(:,i),Sigma(:,:,i));
%end
%%Rescaling (not obligatory)
%r = r'/r_max;
%
%
%end


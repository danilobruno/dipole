function out = genSprings(Start,Target,nbStates,Par)

nbData = 200;

kP = Par.kP; %Initial stiffness gain
kV = Par.kV; %Damping gain
dt = Par.dt; %Time step
alpha = Par.alpha; %Decay factor

nbVar = size(Start,1);

T = nbData*dt;
P = Start;
V = zeros(nbVar,1);
A0 = alpha^2*(Start - Target)/(exp(-alpha*T)+exp(-alpha*T)*alpha*T-1);


Mu_t = linspace(0,nbData*dt,nbStates);
Sigma_t = (nbData*dt/nbStates)*8E-2;

s = 1; %Initialization of decay term
for n=1:nbData
  s = s + (-alpha*s)*dt; %Update of decay term
  t = -log(s)/alpha; %Corresponding time (t=n*dt)
  A = A0*(1-alpha*t)*exp(-alpha*t);
  for i=1:nbStates
    h(i) = gaussPDF(t,Mu_t(i),Sigma_t); %Probability to be in a given state
  end
  H(n,:) = h./sum(h); %Normalization
  Pos(:,n) = P;
  Vel(:,n) = V;
  Acc(:,n) = A;
  V = A*dt + V;
  P = V*dt + P;
end
%Batch least norm solution to find the centers of the states (or primitives) Mu_X (Y=Mu_x*H')
Y =  Acc.*(1/kP) + Pos + Vel.*(kV/kP);
Mu_x = Y*pinv(H'); %Pseudoinverse solution Mu_x = [inv(H'*H)*H'*Y']'

%Compute residuals - Eq.(4)
RI = eye(nbVar,nbVar).*1E-3; %Regularization term for matrix inversion
%Fast computation
for i=1:nbStates
  Sigma_x(:,:,i) = 5*cov(((Y-repmat(Mu_x(:,i),1,nbData))*diag(H(:,i)))');
  Wp(:,:,i) = inv(Sigma_x(:,:,i)+RI); %Use variation information to determine stiffness
end

%Rescale Sigma to allow lateral movement
%for i=1:nbStates
%  [eigV(:,:,i),Dtmp] = eig(Sigma_x(:,:,i)); %Eigencomponents decomposition
%  lambda(:,i) = diag(Dtmp); 
%end
%LL = max(lambda,1);
%for i=1:nbStates
%	Sigma_x(:,:,i) = eigV(:,:,i)*diag(LL(i)/3*ones(1,nbVar))*inv(eigV(:,:,i));
%end
out.Mu = [Mu_t;Mu_x];
for i=1:nbStates
	out.Sigma(:,:,i) = [Sigma_t , zeros(1,nbVar) ; zeros(nbVar,1) , Sigma_x(:,:,i) + eye(nbVar)];
end

end

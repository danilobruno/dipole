function results =  rldpVec(mu0,sigma0,nbEpisodes,nbInitialPoints,rewardFunction,choiceAlg,choiceStart,Min,Max,pars)


%RLDP Implements a research of multioptimal policy starting from given 
%				parameters and with a given starting exploration noise
%
%Notes:
%	Uses Dirichlet processes with Neal algorithm 8
%
%	Reward is treated as an additional dimension for clustering
%	
%	Samples new exploration points using DP

%Author:		Danilo Bruno
%Date:			September, 2012
%
%INPUT:
%
%	choiceAlg 		1:	POWER	2: 	CEM
%	choiceStart		1:	Local	2: 	Global
%
%OUTPUT:
%
%	structure with results
%
%% Algorithm:
%
%	- nbE points are sampled around the initial point with the given exploration noise
%
%	- Points and rewards for points are put together into a unique vector, on which clustering is performed
%	
%	for episode = 1:nbEpidodes
%
%		- DP clustering is performed of the points, determining K(episode) policies
%
%		- 10 POWER steps are performed 
%			- at each step a new point is sampled from each policy and added to the previous ones
%			- only the best nP points are kept for each cluster at each step
%			- POWER policy update is performed
%
%	end
%	
%%%%%
%
%N.B. 
%- The total number of RL episodes in 10*nbEpisodes
%- The total number of evaluated rewards is nP + sum_{episode=1}^nbE 10*K(episode) 

%% Parameters
%

disp('DIPOLE runninG....');
%Space parameters
nbVar = size(sigma0,1);

totEvalRewards = 0;


%RL Parameters
nbE = nbInitialPoints;			%Number of points to sample at the beginning
nbPointsRegr = nbE;	
%DP Parameters
alpha = 1;		%Alpha parameter of DP
nbIters = 100;	%Iterations of DP
m = 3;			%Number of new clusters
%Priors for DP
r0 = rewardEval(mu0,str2func(rewardFunction),pars);	%Evaluate reward of starting point for later DP evaluation
nbRew = size(r0,1);
Sigmastart = sigma0;
muStart = mu0;
lambda0 = sigma0;
nu0 = nbVar +2 + nbRew;
k0 = 1;
PriorReward = 0.5*eye(nbRew);
%Number of total variables (with added reward)
nbVarAll = nbVar +nbRew;


%% Algorithm
%
%
tic

if choiceStart == 1
	%Sample nbE points from the first exploration gaussian
	P = mvnrnd(mu0,lambda0,nbE)';	%Sample from normal
	r = zeros(nbRew,nbE);
	for i=1:nbE
		r(:,i) = rewardEval(P(:,i),str2func(rewardFunction),pars);				%Evaluate reward
		totEvalRewards = totEvalRewards + 1;
	end
elseif choiceStart == 2
	%Sample random points from the whole space (at least 20)
	P = repmat(Min,1,nbE) + repmat((Max - Min),1,nbE) .* rand(nbVar,nbE);	%Ramdom policy
	r = zeros(nbRew,nbE);
	for i=1:nbE
		r(:,i) = rewardEval(P(:,i),str2func(rewardFunction),pars);							%Evaluate reward
		totEvalRewards = totEvalRewards + 1;
	end
end


%Put data and reward into a unique vector

y = [P;r];
yAll = y;		%For plotting

%Set initial values for DP (1 cluster for all)
K = 1;					%Total number of clusters
MU = mean(y')';
SIGMA = cov(y');
%convFact = 0;
NUM = nbE;
c= ones(1,nbE); 
%Keep data
results(1).Mu = MU;
results(1).Sigma = SIGMA;
results(1).totEvalRewards = totEvalRewards;
index = 1;
minsigma = SIGMA/1000;
SIGMA = SIGMA + 0.001*eye(nbVarAll);

N = nbE;				%Total number of points to cluster
nP = 5;
nbE = 1;				%New points to sample from now on
%RL episodes
for episode = 1:nbEpisodes
%	Cluster with DP
%
	for iter = 1:nbIters 
		
		%step 1: iteration over all the elements
		for i=1:N
			 y_ = y(:,i);
			%if c(i) is a singleton
			 if NUM(c(i)) == 1
				 %number of cluster values different from c(i)
				 K_ = K - 1;
				 %put the singleton cluster in the last position
				 oldind = c(i);
				 mutmp = MU(:,oldind);
				 sigmatmp = SIGMA(:,:,oldind);
				 NUM(oldind) = NUM(K);
				 MU(:,oldind) = MU(:,K);
				 SIGMA(:,:,oldind) = SIGMA(:,:,K);
				 MU(:,K) = mutmp;
				 SIGMA(:,:,K) = sigmatmp;
				 NUM(K) = 1;
				 c(c==K)= oldind;
				 c(i) = K;             
			 else
				 %number of cluster values different from c(i)
				 K_ = K;                 
			 end      
			 %total clusters to produce
			 h = K_ + m;
			 %sample parameters for the auxiliary clusters
			 for jj=K+1:h
				 %cl(jj).mu = mvnrnd(mu0,sigma0);
				 %muP = Min + (Max - Min) .* rand(2,1); %mu sampled from the uniform
				 [muP sigmaP] = PriorSampleNormIWish(mu0,k0,lambda0,nu0);
				 MU(:,jj) = [muP';rand(nbRew,1)];
				 SIGMA(:,:,jj) = [sigmaP zeros(nbVar,nbRew) ; zeros(nbRew,nbVar) PriorReward];
				 NUM(jj) = 0;	%auxililary clusters are empty
			 end
			 %resample c(i) with probability of dp
			 %%%build probabilities
			 %%determine probabilities for the first k_ clusters
			 p = zeros(1,h);
			 for k=1:K_
				 p(k) = NUM(k);
				 if k==c(i)
					 p(k) = p(k)-1;
				 end
				 p(k) = MU(end,k)*p(k)/(N-1+alpha) * mvnpdf(y_,MU(:,k),SIGMA(:,:,k));
			 end
			 %determine probabilities for the auxiliary clusters
			 for k=K_+1:h
				 p(k) = MU(end,k)*(alpha/m)/(N-1+alpha)*mvnpdf(y_,MU(:,k),SIGMA(:,:,k));
			 end
			 %%%sample cluster for ith data
			 a = catsample(p);
			 %move point to the new cluster
			 NUM(c(i)) = NUM(c(i)) -1;%eliminate from old
			 NUM(a) = NUM(a) + 1;%put into new
			 c(i) = a; %reassign label
			 
			%sweep clusters to eliminate empty ones
			%put the non empty clusters in a new structure cl_
			%put the new cluster labels in a new variable c_ 
			%new number of clusters
			sel = NUM == 0; %The zeros are the non empty clusters
			idx = find(NUM);%Indexes of non empty clusters 
			MU = MU(:,sel==0);
			SIGMA = SIGMA(:,:,sel==0);
			NUM = NUM(sel==0);
			K = length(idx);
			for k=1:K
				c(c==idx(k)) = k;
			end
		end
		%step 2: draw new values for the parameters of each cluster
		for k=1:K
			ycl = y(:,c==k); %data belonging to cluster
			cn = NUM(k);    %number of objects in cluster
			%new experimental mean
			xx = zeros(nbVarAll,1);
			for jj=1:cn
				xx = xx + ycl(:,jj);
			end
			xx = xx/cn;
			%new experimental covariance
			S = zeros(nbVarAll);
			for j=1:cn
				S = S + (ycl(:,j) - xx)*(ycl(:,j) - xx)';
			end
			%posterior parameters for iwish
			lambda0_ = [lambda0 zeros(nbVar,nbRew) ; zeros(nbRew,nbVar) PriorReward] + S + ... 
				(k0*cn)/(k0+cn)*(xx - [mu0;r0])*(xx-[mu0;r0])';
			nu0_ = nu0 + cn;
			SIGMA(:,:,k) = iwishrnd(lambda0_,nu0_); 
			MU(:,k) = xx;
			%sigma0_ = inv(inv(sigma0)+cn*inv(cl(k).sigma));
			%mu0_ = sigma0_*(cn*inv(cl(k).sigma)*xx' + inv(sigma0)*mu0');
			%cl(k).mu = mvnrnd(mu0_,sigma0_);
			%cl(k).mu = xx;
		end
	end
	nbIters = 30;	%Different only for the first iteration

	for rlstep=1:10
	%	Sample 1  point for each cluster at each iteration 
		PNoisy = zeros(nbVar,K);
		cNoisy = zeros(1,K);
		for k=1:K
			PNoisy(:,k) = MU([1:nbVar],k);
			cNoisy(k) = k;
			NUM(k) = NUM(k) +1;
		end
		%end
		%Add to previous P
		rNoisy = zeros(nbRew,size(PNoisy,2));
		for i=1:size(PNoisy,2)
			rNoisy(:,i) = rewardEval(PNoisy(:,i),str2func(rewardFunction),pars);
			totEvalRewards = totEvalRewards + 1;
		end
		P = [P PNoisy];
		r = [r rNoisy];
		c = [c cNoisy];

		y = [P;r];		%Put parameters and rewards into a unique vector

		yNoisy = [PNoisy;rNoisy];
		yAll = [yAll yNoisy];
		
		N=0;
		ynew=[];
		cnew=[];
		for k=1:K
			ycl = y(:,c==k); %data belonging to cluster
			cn = NUM(k);    %number of objects in cluster
			rcl = ycl(end-nbRew+1:end,:);
			%prob = mvnpdf(ycl(1:nbVar,:)',MU([1:nbVar],k)',SIGMA([1:nbVar],[1:nbVar],k));
			prob = mvncdf(ycl(nbVar+1:end,:)',MU([nbVar+1:end],k)',SIGMA([nbVar+1:end],[nbVar+1:end],k));
			[probSrt,idSrt] = sort(prob,'descend');
			nbP = min(length(idSrt),nP);
			yclTmp = ycl(:,idSrt(1:nbP));
			rTmp = rcl(:,idSrt(1:nbP));
			probTmp = probSrt(1:nbP);
			%eTmp = yclTmp - repmat(MU(:,k),1,nbP);
			%Standard udpate of the policy
			%
			%Update the parameters of each cluster with POWER
			%if sum(rTmp) > 0
			%	MU(:,k) = MU(:,k) + eTmp*rTmp'/sum(rTmp);
				%SIGMA(:,:,k) = (eTmp*diag(rTmp)*eTmp')/sum(rTmp) + minsigma;
			%else
			%	MU(:,k) = MU(:,k);
			%	SIGMA(:,:,k) = SIGMA(:,:,k) + minsigma;
			%end
			%Only keep points with higher reward
			ynew = [ynew yclTmp];
			cnew = [cnew k*ones(1,nbP)];
			NUM(k) = nbP;
			N = N+nbP;
			%Update policy with GMR
			MU([1:end-nbRew],k) = MU([1:end-nbRew],k) + SIGMA([1:end-nbRew],nbVar+1:end,k) * inv(SIGMA(nbVar+1:end,nbVar+1:end,k)) * (1 - MU(nbVar+1:end,k));
			MU(nbVar+1:end,k) = mean(yclTmp(nbVar+1:end,:),2);
			SIGMA([1:nbVar],[1:nbVar],k) = SIGMA([1:nbVar],[1:nbVar],k) - SIGMA([1:nbVar],nbVar+1:end,k)*inv(SIGMA(nbVar+1:end,nbVar+1:end,k))*SIGMA(nbVar+1:end,[1:nbVar],k) + 0.01*eye(nbVar);
			SIGMA(:,:,k) = cov(yclTmp') + 0.01*eye(nbVarAll);
		end
		y = ynew;
		c = cnew;
		P = y(1:nbVar,:);
		r = y(nbVar+1:end,:);
		index = index +1;
		results(index).Mu = MU;
		results(index).Sigma = SIGMA;
		pr = zeros(1,size(SIGMA,3));
		for k=1:size(SIGMA,3)
			for j = 1:nbRew
				aa(j,k) = MU(nbVar+j,k) + (1-MU(nbVar+j,k))*MU(nbVar+j,k)^2/SIGMA(nbVar+j,nbVar+j,k);		%Parameters of beta distribution corresponding to r dist
				bb(j,k) = (1 - MU(nbVar+j,k)) + (1-MU(nbVar+j,k))^2*MU(nbVar+j,k)/SIGMA(nbVar+j,nbVar+j,k);
				pr(j,k) = 1 - betacdf(0.95,aa(j,k),bb(j,k)); %Evaluate probability to get a reward in the last 20ile
			end
		end
		results(index).probRew = pr;
		results(index).totEvalRewards = totEvalRewards;

%		for k=1:K
%			if MU(end,k) == 1001
%				MU = MU(:,k);
%				SIGMA = 0.1 * eye(nbVarAll);
%				K = 1;
%				y = y(:,c==k);
%				N = NUM(k);
%				NUM = N;
%				break;
%			end
%		end


		fprintf('Episode : %d ; Number of optimal policies: %d \n',index,K);
		fprintf('Rew:\t ');
		for k=1:K
			fprintf('%d\t',MU(end-nbRew+1:end,k));
		end
		fprintf('\n');
		%fprintf('Convergence probability :\t ');
		%for k=1:K
		%	fprintf('%d\t',convFact(k) > 0.01 );
		%end
		%fprintf('\n');
		fprintf('Prob:\t ');
		for k=1:K
			fprintf('%d\t',pr(:,k));
		end
		fprintf('\n');
		fprintf('Value:\t ');
		for k=1:K
			fprintf('%d\t',pr(:,k).*MU(end-nbRew+1:end,k));
		end
		fprintf('\n');
	end

	nP = 5;
	
	%Update priors
	%Calculate mean among clusters
	muBar = zeros(nbVar,1);
	sigmaBar = zeros(nbVar);
	for k=1:K
		muBar = muBar + MU([1:nbVar],k);
	end
	muBar = muBar/K;
	%Calculate covariance among clusters
	for k=1:K
		sigmaBar = sigmaBar + (MU([1:nbVar],k)-muBar)*(MU([1:nbVar],k)-muBar)';
	end
	%Update priors
	mu0_ = k0/(k0+K)* mu0 + K/(k0+K) * muBar;
	k0_ = k0 + K;
	nu0_ = nu0 + K;
	lambda0_ = lambda0 + sigmaBar + (k0*K)/(k0+K)* (muBar- mu0)*(muBar - mu0)';
	%[mu0Tmp lambda0] = PriorSampleNormIWish(mu0_,k0_,lambda0_,nu0_);
	mu0 = mu0_;
	%lambda0 = lambda0_;
	%nu0 = nu0_;
	%k0 = k0_;


end
%
%

results(index).totalTime = toc;
results(index).totEvalRewards = totEvalRewards;


end


%Sample from multinomial with probability vector p
function a = catsample(p)
% Samples from the multinomial with probabilities p
	z = rand(1,1);
	p = p/sum(p);
	t = cumsum(p);
	%a contains the cluster where the point fits
	a = sum(repmat(z,1,numel(p)) >= repmat(t,1,1),2)+1;
end
 

function [ mu,sigma ] = PriorSampleNormIWish( mu0,k0,lambda0,nu0 )
%PRIORSAMPLENORMIWISH samples mu and sigma from normal inverse wishart
%   
% needs: statistics toolbox
%
% Author: Danilo Bruno
% Date: May 2012

sigma = iwishrnd((lambda0),nu0);
mu = mvnrnd(mu0,sigma/k0);


end


function r = rewardEval(p,reward,pars)
	
		r = reward(p,pars);

end
%function r = rewardEval(p)
%Mu = zeros(2,3);
%Sigma = zeros(2,2,3);
%i=0;
%Mu(:,1)=[3;3]; Mu(:,2)=[7;7]; Mu(:,3)=[1.5;8.5];
%Sigma(:,:,1)=diag([1,9]); Sigma(:,:,2)=diag([9,1]); Sigma(:,:,3)=diag([.5,.5]);
%Priors = [1 1 0.6];
%Priors = Priors/sum(Priors);
%r = zeros(size(p,2),1);
%r_max = 0;
%for i=1:size(Mu,2)
%  r = r + Priors(i) * gaussPDF(p,Mu(:,i),Sigma(:,:,i));
%  r_max = r_max + Priors(i) * gaussPDF(Mu(:,i),Mu(:,i),Sigma(:,:,i));
%end
%%Rescaling (not obligatory)
%r = r'/r_max;
%
%
%end


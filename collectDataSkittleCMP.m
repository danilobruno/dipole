function collectDataSkittle()

needsData = 1;

if needsData == 1
	mu0 = [1;5];		%Starting point

	sigma0 = 0.3*eye(2);	%Initial exploration noise and prior for DP

	%sigma0 = [0 0 ; pi 10];

	nbEpisodes = 60;	
	nbE = 10;			%Number of points to sample at the beginning
	totIters = 10;
	%results = rldp(mu0,sigma0,nbEpisodes,nbE,'SkittleReward',2,1);
	pars = 0;
	for iter = 1:totIters
		results = rldp(mu0,sigma0,nbEpisodes,4*nbE,'SkittleReward',1,1,[0;0],[pi;10],pars);
		resultsnew = rldpVec(mu0,sigma0,nbEpisodes,4*nbE,'SkittleReward',1,1,[0;0],[pi;10],pars);
		for i=1:length(results)
			bestres(iter,i) = max(results(i).Mu(end,:));
			avgres(iter,i) = mean(results(i).Mu(end,:));
			trials(iter,i) = results(i).totEvalRewards;
			num(iter,i) = size(results(i).Mu,2);
		end
		for i=1:length(resultsnew)
			bestresnew(iter,i) = max(resultsnew(i).Mu(end,:));
			avgresnew(iter,i) = mean(resultsnew(i).Mu(end,:));
			trialsnew(iter,i) = resultsnew(i).totEvalRewards;
			numnew(iter,i) = size(resultsnew(i).Mu,2);
		end
	end
	save('data\SKData.mat','bestres','avgres','trials','num','bestresnew','avgresnew','trialsnew','numnew');
else
	load('data\SKData.mat','bestres','avgres','trials','num','bestresnew','avgresnew','trialsnew','numnew');
end


set(0,'DefaultLineLineWidth',2)
bestRewards = mean(bestres,1);
avgRewards = mean(avgres,1);
avgnum = mean(num,1);
meantrials = mean(trials,1);
bestRewardsnew = mean(bestresnew,1);
avgRewardsnew = mean(avgresnew,1);
avgnumnew = mean(numnew,1);
meantrialsnew = mean(trialsnew,1);



totBest = mean(bestres,1);
totAvg = mean(avgres,1);
tottrials = mean(trials,1);
covBest = sqrt(var(bestres));
%covrPow = sqrt(var(rPow))/3;
covAvg = sqrt(var(avgres));
covnum = sqrt(var(num));

totBestnew = mean(bestresnew,1);
totAvgnew = mean(avgresnew,1);
tottrialsnew = mean(trialsnew,1);
covBestnew = sqrt(var(bestresnew));
%covrPow = sqrt(var(rPow))/3;
covAvgnew = sqrt(var(avgresnew));
covnumnew = sqrt(var(numnew));

set(0,'DefaultLineLineWidth',2)
figure()
%plot(30*[1:length(totPower)],totPower,'color','blue');hold on
%ymin = totrPow - covrPow;
%ymax = totrPow + covrPow;
%patch([[1:length(totrPow)] fliplr([1:length(totrPow)])],[ymin fliplr(ymax)],[0.7 0.7 1 ],'EdgeColor','none');


ymin = totAvg - covAvg;
ymax = totAvg + covAvg;
patch([tottrials fliplr(tottrials)],[ymin fliplr(ymax)],[1 0.7 0.7 ],'EdgeColor','none');hold on
plot(tottrials,totAvg,'color',[0.4 0 0]);

ymin = totAvgnew - covAvgnew;
ymax = totAvgnew + covAvgnew;
patch([tottrialsnew fliplr(tottrialsnew)],[ymin fliplr(ymax)],[0.7 1 0.7 ],'EdgeColor','none');hold on
plot(tottrialsnew,totAvgnew,'color',[0 0.4 0]);
%plot([1:length(totrPow)],totrPow,'color',[0 0 0.4]);hold on


figure
ymin = totBest - covBest;
ymax = totBest + covBest;
patch([tottrials fliplr(tottrials)],[ymin fliplr(ymax)],[1 0.7 0.7 ],'EdgeColor','none');hold on
%plot([1:length(totrPow)],totrPow,'color',[0 0 0.4]);
plot(tottrials,totBest,'color',[0.4 0 0]);

ymin = totBestnew - covBestnew;
ymax = totBestnew + covBestnew;
patch([tottrialsnew fliplr(tottrialsnew)],[ymin fliplr(ymax)],[ 0.7 1 0.7 ],'EdgeColor','none');hold on
%plot([1:length(totrPow)],totrPow,'color',[0 0 0.4]);
plot(tottrialsnew,totBestnew,'color',[0 0.4 0]);

return
title('Best and Average rewards','fontsize',15);
xlabel('Trials','fontsize',15)
ylabel('Reward','fontsize',15)
legend('Average','Best','Location','SouthEast');


return

figure()

title('Average number of policies','fontsize',15);
xlabel('Trials','fontsize',15)
ylabel('Policies','fontsize',15)
ymin = avgnum - covnum;
ymax = avgnum + covnum;
patch([meantrials fliplr(meantrials)],[ymin fliplr(ymax)],[0.7 1 0.7 ],'EdgeColor','none');hold on
plot(meantrials,avgnum,'color',[0 0.4 0]);

return


%plot(30*[1:length(totPower)],totPower,'color','blue');hold on
subplot(1,2,2)
%ymin = totrPow - covrPow;
%ymax = totrPow + covrPow;
%patch([[1:length(totrPow)] fliplr([1:length(totrPow)])],[ymin fliplr(ymax)],[0.7 0.7 1 ],'EdgeColor','none');
ymin = totAvg - covAvg;
ymax = totAvg + covAvg;
patch([meantrials fliplr(meantrials)],[ymin fliplr(ymax)],[0.7 1 0.7 ],'EdgeColor','none');hold on

%plot([1:length(totrPow)],totrPow,'color',[0 0 0.4]);hold on
plot(tottrials,totAvg,'color',[0 0.4 0]);
title('POWER v.s. DIPOLE - Reward of average policy','fontsize',15);
xlabel('Trials','fontsize',15)
ylabel('Reward','fontsize',15)
legend('POWER','DIPOLE','Location','SouthEast');













return




figure()
plot(meantrials,bestRewards,'color','red');hold on
title('DIPOLE average performance on 30 trials','fontsize',15);
xlabel('Episode','fontsize',15)
ylabel('Reward','fontsize',15)
plot(meantrials,avgRewards,'color','black');
legend('Best policy','Average','Location','SouthEast');

figure()
plot(meantrials,avgnum);
title('Average number of policies on 30 trials','fontsize',15);
xlabel('Episode','fontsize',15)
ylabel('K','fontsize',15)

end


function [r,w,x] = simulateTrajSkittle(p,xg,needsPlot,colTmp)

alpha=p(1);
dalpha=p(2);

nbData = 100;
nbVar = 2;
%xg = [0.3;1.4]; %target position

m = 0.1; %mass of the ball 
k = 1; %spring constant
tau = 20; %relaxation time
omega = (k/m - 1/tau^2)^0.5; %frequency
xp0 = [0;-1.5]; %axis of rotation of the paddle
len = 0.4; %length of the paddle
r_post = 0.25; %radius of the center post
r_ball = 0.15; %radius of the ball
r_skittle = 0.05; %radius of the skittle
vnorm = dalpha * len; %velocity norm
v = [sin(alpha);cos(alpha)] * vnorm * sign(alpha); %velocity at release
xr = xp0 + [cos(alpha);-sin(alpha)] * len; %release point
E = (v.^2 * m + xr.^2 * k) * 0.5; %potential energy
A = (E*2/k).^0.5; %amplitude
phi = asin(xr./A); %phase difference

t = linspace(0,1.5,nbData);
w = 0;
r = [999;999];
for i=1:nbData
  x(:,i) = A .* sin(phi + omega * t(i)) * exp(-t(i)/tau);
  %collision with center post
  if norm(x(:,i))<r_post+r_ball
    minDist = .5;
    x(:,i:nbData) = repmat(x(:,i-1),1,nbData-i+1);
    break;
  end
  %Find closest point to the target (-> large weight)
  invNormTmp = exp(-10*norm(x(:,i)-xg));
  if invNormTmp>w
    w = invNormTmp;
    r = x(:,i);
  end
end

%minDist = min(minDist,0.5); %For display purpose

if needsPlot==1
  %Plot center post
  tp = linspace(0,2*pi,30);
  patch(cos(tp)*r_post,sin(tp)*r_post,[.7 .7 .7]);hold on
%  plot([0 r_post],[0 0],'k-');
  %plot(0,0,'k+');
  %Plot ball (for last point only)
  patch(x(1,end)+cos(tp)*r_ball, x(2,end)+sin(tp)*r_ball, min(colTmp+0.1,1));
%  plot([x(1,end) x(1,end)+r_ball],[x(2,end) x(2,end)],'k-');
  %Plot trajectory
  plot(x(1,:),x(2,:),'.','color',colTmp);
  %Draw arrow direction
  xe = xr + v * 1.9E-1;
  plot([xr(1) xe(1)],[xr(2) xe(2)],'-','color',min(colTmp+0.2,1),'linewidth',1);
  %Draw arrow head
  xh(:,1) = xr + v*2E-1;
  xh(:,2) = xh(:,1) - v*1E-1/norm(v) + [-v(2);v(1)]*.5E-1/norm(v);
  xh(:,3) = xh(:,1) - v*1E-1/norm(v) - [-v(2);v(1)]*.5E-1/norm(v);
  xh(:,4) = xr + v*2E-1;
  patch(xh(1,:),xh(2,:),max(colTmp-0.1,0),'linestyle','none');
  %Plot paddle
  tp = linspace(0,alpha,30);
  plot(xp0(1,end)+cos(tp)*0.08, xp0(2,end)-sin(tp)*0.08,'-','color',max(colTmp-0.1,0));
  plot([xp0(1) xp0(1)+.2],[xp0(2) xp0(2)],'-','color',max(colTmp-0.1,0),'linewidth',1);
  plot(xp0(1),xp0(2),'.','markersize',12,'color',max(colTmp-0.1,0));
  plot(xr(1),xr(2),'.','markersize',12,'color',max(colTmp-0.1,0));
  plot([xp0(1) xr(1)],[xp0(2) xr(2)],'-','color',max(colTmp-0.1,0),'linewidth',1.5);
  %Plot target
  plot(xg(1),xg(2),'+','markersize',12,'linewidth',4,'color',[0 0 0]);

%   %Closest point to the target
%   plot(r(1),r(2),'.','color',[0 .4 0]);
%   plot([r(1) xg(1)],[r(2) xg(2)],'-','color',[0 .4 0],'linewidth',.5);
  
%   %Various text annotations
%   text(0,0,'$\mathbf{x}_p \mathbf{x}_c \mathbf{x}_r \dot{\mathbf{x}}_r \mathbf{x}_t \mathbf{x}_e$','interpreter','latex','fontsize',16); 
%   text(xp0(1),xp0(2),'$\alpha,\dot{\alpha} r_p r_b r_c$','interpreter','latex','fontsize',16); 
  
  axis([-2.3 2.3 -2.3 2.3]); axis square;
  %axis equal;
  set(gca,'xtick',[],'ytick',[]);
  xlabel('$x_1$','interpreter','latex','fontsize',25); 
  ylabel('$x_2$','interpreter','latex','fontsize',25);
end

end

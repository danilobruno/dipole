function demoSkittle1()

needsData = 1;

if needsData == 1
	mu0 = [1;5];		%Starting point

	sigma0 = 0.3*eye(2);	%Initial exploration noise and prior for DP

	%sigma0 = [0 0 ; pi 10];

	nbEpisodes = 150;	
	nbE = 10;			%Number of points to sample at the beginning

	%results = rldp(mu0,sigma0,nbEpisodes,nbE,'SkittleReward',2,1);
	results = rldpVec(mu0,sigma0,nbEpisodes,4*nbE,'SkittleReward',3,1,[0;0],[pi;10],0);

	save('data/resultsSkittle.mat','results');

	figure();
	hold on
	for i=1:length(results)
		for k=1:size(results(i).Mu,2)
			plot(i,results(i).Mu(end,k),'.')
		end
	end




else
	load('data/resultsSkittle.mat','results');
end

%figure()
%for k = 1:size(results(end).Mu,2)
%	simulateTrajSkittle(results(end).Mu([1:2],k)',[0;1.6],1,[0.3 0 0 ]);	
%end
bestres = zeros(1,length(results));
avgres = zeros(1,length(results));
trials = zeros(1,length(results));
num = zeros(1,length(results));
%numGood = zeros(1,length(results));
for i=1:length(results)
	bestres(i) = max(results(i).Mu(end,:));
	avgres(i) = mean(results(i).Mu(end,:));
	trials(i) = results(i).totEvalRewards;
	num(i) = size(results(i).Mu,2);
%	for k=1:size(results(i).Mu,2)
%		if results(i).Mu(end,k) > 0.85 
%			numGood(i) = numGood(i) +1;
%		end
%	end
end
set(0,'DefaultLineLineWidth',2)
figure()
%plot([1:length(results)],bestres);
plot(trials,bestres,'color','red');hold on
title('Reward DIPOLE','fontsize',15);
xlabel('Episode','fontsize',15)
ylabel('Reward','fontsize',15)
%plot([1:length(results)],avgres);
plot(trials,avgres,'color','black');
title('Average reward DIPOLE','fontsize',15);
xlabel('Episode','fontsize',15)
ylabel('Reward','fontsize',15)
legend('Best policy','Average','Location','SouthEast');
%

%figure()
%%plot([1:length(results)],bestres);
%plot(trials,num);
%title('Number of policies','fontsize',15);
%xlabel('Episode','fontsize',15)
%ylabel('K','fontsize',15)
%figure()
%%plot([1:length(results)],avgres);
%plot(trials,numGood);
%title('Number of good policies','fontsize',15);
%xlabel('Episode','fontsize',15)
%ylabel('K','fontsize',15)

%figure
%resolution = 20;
%xxx = linspace(0,2.8,resolution);
%yyy = linspace(3,10,resolution);
%%xxx = linspace(1.9,2.1,200);
%%yyy = linspace(4,5.5,200);
%for i=1:resolution
%	for j=1:resolution
%		zzz(j,i) = SkittleReward([xxx(i);yyy(j)]);
%	end
%end
%
%%contour(xxx,yyy,zzz,400)
%colormap('Gray')
%pcolor(xxx,yyy,zzz);
%shading interp
%hold on
%plotGMM(results(end).Mu([1:2],:),results(end).Sigma([1:2],[1:2],:),[1,0,0],1);
%title('Reward function and GMM of selected policies')
%xlabel('\theta_1')
%ylabel('\theta_2')
%

Mu = results(end).Mu;
Sigma = results(end).Sigma;

nbStates = size(Sigma,3);

v = [1,2];
figure
subplot(1,3,1)
%resolution = 200;
%xxx = linspace(0,2.8,resolution);
%yyy = linspace(3,10,resolution);
%%xxx = linspace(1.9,2.1,200);
%%yyy = linspace(4,5.5,200);
%for i=1:resolution
%	for j=1:resolution
%		zzz(j,i) = SkittleReward([xxx(i);yyy(j)]);
%	end
%end

%save('data\heatmap.mat','xxx','yyy','zzz');
load('data\heatmap.mat','xxx','yyy','zzz');

%contour(xxx,yyy,zzz,400)
colormap('Gray')
pcolor(xxx,yyy,zzz);
shading interp
hold on
plotGMM(results(end).Mu([1:2],:),results(end).Sigma([1:2],[1:2],:),[1,0,0],1);
title('Reward function and GMM of selected policies')
xlabel('\theta_1')
ylabel('\theta_2')

cmap = colormap(jet);
for k=1:nbStates
	plotGMM(Mu(v,k),Sigma(v,v,k),cmap(5*k,:),1);hold on
end

for i=1:nbStates
	ll(i) = 1/10*Mu(end,i);
	kk(i) = ll(i)/Sigma(end,end,i);
	SS(:,:,i) = Sigma(:,:,i) * kk(i);
end
%plotGMM(Mu(v,:),SS(v,v,:),[0 0 0],1);hold on

v = [1,3];



subplot(1,3,2)
for k=1:nbStates
	plotGMM(Mu(v,k),Sigma(v,v,k),cmap(5*k,:),1);hold on
end
%plotGMM(Mu(v,:),SS(v,v,:),[0 0 0],1);hold on
legend('1','2','3','4','5');

subplot(1,3,3)
v = [2,3];
for k=1:nbStates
	plotGMM(Mu(v,k),Sigma(v,v,k),cmap(5*k,:),1);hold on
end
%plotGMM(Mu(v,:),SS(v,v,:),[0 0 0],1);hold on
legend('1','2','3','4','5');
figure
for k=1:nbStates
	Res(k,:) = [det(Sigma([1:2],[1:2],k)) ,Sigma(end,end,k) , Sigma(end,end,k) - Sigma(end,[1:2],k)*inv(Sigma([1:2],[1:2],k))*Sigma([1:2],end,k) , (Sigma(end,end,k) - Sigma(end,[1:2],k)*inv(Sigma([1:2],[1:2],k))*Sigma([1:2],end,k))/Sigma(end,end,k) , Mu(end,k)];
end
cmap = colormap(lines); 
for i=1:5
	RR = Res(:,i);
	plot([1:nbStates],RR,'color',cmap(i,:));hold on
end
legend('Space cov','Rew cov','Cond cov','Proposed factor','Rew');
return
figure
tot = 1000;
t = linspace(0,1,tot);
for k=1:nbStates
	aa(k) = Mu(end,k) + (1-Mu(end,k))*Mu(end,k)^2/Sigma(end,end,k);
	bb(k) = (1 - Mu(end,k)) + (1-Mu(end,k))^2*Mu(end,k)/Sigma(end,end,k);
	f(:,k) = betacdf(t,aa(k),bb(k));
	plot(t,f(:,k),'color',cmap(k,:));hold on
	pr(k) = f(tot,k)-f(0.95*tot,k);
end
legend('1','2','3','4','5');
pr

figure
rrr = zeros(1,length(results));
for i=1:length(results)
	for k=1:size(results(i).Sigma,3)
		rrr(i) = rrr(i) + (results(i).Sigma(end,end,k) - results(i).Sigma(end,[1:2],k)*inv(results(i).Sigma([1:2],[1:2],k))*results(i).Sigma([1:2],end,k));
	end
	rrr(i) = rrr(i)/size(results(i).Sigma,3);
end
plot(trials,(rrr));


end


function [r,w,x] = simulateTrajSkittle(p,xg,needsPlot,colTmp)

alpha=p(1);
dalpha=p(2);

nbData = 700;
nbVar = 2;
%xg = [0.3;1.4]; %target position

m = 0.1; %mass of the ball 
k = 1; %spring constant
tau = 20; %relaxation time
omega = (k/m - 1/tau^2)^0.5; %frequency
xp0 = [0;-1.5]; %axis of rotation of the paddle
len = 0.4; %length of the paddle
r_post = 0.25; %radius of the center post
r_ball = 0.15; %radius of the ball
r_skittle = 0.05; %radius of the skittle
vnorm = dalpha * len; %velocity norm
v = [sin(alpha);cos(alpha)] * vnorm * sign(alpha); %velocity at release
xr = xp0 + [cos(alpha);-sin(alpha)] * len; %release point
E = (v.^2 * m + xr.^2 * k) * 0.5; %potential energy
A = (E*2/k).^0.5; %amplitude
phi = asin(xr./A); %phase difference

t = linspace(0,1.5,nbData);
w = 0;
r = [999;999];
for i=1:nbData
  x(:,i) = A .* sin(phi + omega * t(i)) * exp(-t(i)/tau);
  %collision with center post
  if norm(x(:,i))<r_post+r_ball
    minDist = .5;
    x(:,i:nbData) = repmat(x(:,i-1),1,nbData-i+1);
    break;
  end
  %Find closest point to the target (-> large weight)
  invNormTmp = exp(-10*norm(x(:,i)-xg));
  if invNormTmp>w
    w = invNormTmp;
    r = x(:,i);
  end
end

%minDist = min(minDist,0.5); %For display purpose

if needsPlot==1
  %Plot center post
  tp = linspace(0,2*pi,30);
  patch(cos(tp)*r_post,sin(tp)*r_post,[.7 .7 .7]);hold on
%  plot([0 r_post],[0 0],'k-');
  %plot(0,0,'k+');
  %Plot ball (for last point only)
  patch(x(1,end)+cos(tp)*r_ball, x(2,end)+sin(tp)*r_ball, min(colTmp+0.1,1));
%  plot([x(1,end) x(1,end)+r_ball],[x(2,end) x(2,end)],'k-');
  %Plot trajectory
  plot(x(1,:),x(2,:),'.','color',colTmp);
  %Draw arrow direction
  xe = xr + v * 1.9E-1;
  plot([xr(1) xe(1)],[xr(2) xe(2)],'-','color',min(colTmp+0.2,1),'linewidth',1);
  %Draw arrow head
  xh(:,1) = xr + v*2E-1;
  xh(:,2) = xh(:,1) - v*1E-1/norm(v) + [-v(2);v(1)]*.5E-1/norm(v);
  xh(:,3) = xh(:,1) - v*1E-1/norm(v) - [-v(2);v(1)]*.5E-1/norm(v);
  xh(:,4) = xr + v*2E-1;
  patch(xh(1,:),xh(2,:),max(colTmp-0.1,0),'linestyle','none');
  %Plot paddle
  tp = linspace(0,alpha,30);
  plot(xp0(1,end)+cos(tp)*0.08, xp0(2,end)-sin(tp)*0.08,'-','color',max(colTmp-0.1,0));
  plot([xp0(1) xp0(1)+.2],[xp0(2) xp0(2)],'-','color',max(colTmp-0.1,0),'linewidth',1);
  plot(xp0(1),xp0(2),'.','markersize',12,'color',max(colTmp-0.1,0));
  plot(xr(1),xr(2),'.','markersize',12,'color',max(colTmp-0.1,0));
  plot([xp0(1) xr(1)],[xp0(2) xr(2)],'-','color',max(colTmp-0.1,0),'linewidth',1.5);
  %Plot target
  plot(xg(1),xg(2),'+','markersize',12,'linewidth',4,'color',[0 0 0]);

%   %Closest point to the target
%   plot(r(1),r(2),'.','color',[0 .4 0]);
%   plot([r(1) xg(1)],[r(2) xg(2)],'-','color',[0 .4 0],'linewidth',.5);
  
%   %Various text annotations
%   text(0,0,'$\mathbf{x}_p \mathbf{x}_c \mathbf{x}_r \dot{\mathbf{x}}_r \mathbf{x}_t \mathbf{x}_e$','interpreter','latex','fontsize',16); 
%   text(xp0(1),xp0(2),'$\alpha,\dot{\alpha} r_p r_b r_c$','interpreter','latex','fontsize',16); 
  
  axis([-2.3 2.3 -2.3 2.3]); axis square;
  %axis equal;
  set(gca,'xtick',[],'ytick',[]);
  xlabel('$x_1$','interpreter','latex','fontsize',25); 
  ylabel('$x_2$','interpreter','latex','fontsize',25);
end

end

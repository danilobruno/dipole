function r = SkittleReward(p,pars) 

xg = [0;1.6];		%Skittle position


rgMin = [0;3]; %Minimum value of data
rgMax = [2.8;10]; %Maximum value of data
for i=1:size(p,2)
	[rTmp,r(1,i)] = simulateTrajSkittle(p(:,i)',xg,0);
	if p(1,i)<rgMin(1) || p(2,i)<rgMin(2) || p(1,i)>rgMax(1) || p(2,i)>rgMax(2)
	  r(1,i)=0;
	end
end


end



function [r,w,x] = simulateTrajSkittle(p,xg,needsPlot,colTmp)

alpha=p(1);
dalpha=p(2);

nbData = 700;
nbVar = 2;
%xg = [0.3;1.4]; %target position

m = 0.1; %mass of the ball 
k = 1; %spring constant
tau = 20; %relaxation time
omega = (k/m - 1/tau^2)^0.5; %frequency
xp0 = [0;-1.5]; %axis of rotation of the paddle
len = 0.4; %length of the paddle
r_post = 0.25; %radius of the center post
r_ball = 0.15; %radius of the ball
r_skittle = 0.05; %radius of the skittle
vnorm = dalpha * len; %velocity norm
v = [sin(alpha);cos(alpha)] * vnorm * sign(alpha); %velocity at release
xr = xp0 + [cos(alpha);-sin(alpha)] * len; %release point
E = (v.^2 * m + xr.^2 * k) * 0.5; %potential energy
A = (E*2/k).^0.5; %amplitude
phi = asin(xr./A); %phase difference

t = linspace(0,1.5,nbData);
w = 0;
r = [999;999];
for i=1:nbData
  x(:,i) = A .* sin(phi + omega * t(i)) * exp(-t(i)/tau);
  %collision with center post
  if norm(x(:,i))<r_post+r_ball
    minDist = .5;
    x(:,i:nbData) = repmat(x(:,i-1),1,nbData-i+1);
    break;
  end
  %Find closest point to the target (-> large weight)
  invNormTmp = exp(-10*norm(x(:,i)-xg));
  if invNormTmp>w
    w = invNormTmp;
    r = x(:,i);
  end
end

%minDist = min(minDist,0.5); %For display purpose

if needsPlot==1
  %Plot center post
  tp = linspace(0,2*pi,30);
  patch(cos(tp)*r_post,sin(tp)*r_post,[.7 .7 .7]);hold on
%  plot([0 r_post],[0 0],'k-');
  %plot(0,0,'k+');
  %Plot ball (for last point only)
  patch(x(1,end)+cos(tp)*r_ball, x(2,end)+sin(tp)*r_ball, min(colTmp+0.1,1));
%  plot([x(1,end) x(1,end)+r_ball],[x(2,end) x(2,end)],'k-');
  %Plot trajectory
  plot(x(1,:),x(2,:),'.','color',colTmp);
  %Draw arrow direction
  xe = xr + v * 1.9E-1;
  plot([xr(1) xe(1)],[xr(2) xe(2)],'-','color',min(colTmp+0.2,1),'linewidth',1);
  %Draw arrow head
  xh(:,1) = xr + v*2E-1;
  xh(:,2) = xh(:,1) - v*1E-1/norm(v) + [-v(2);v(1)]*.5E-1/norm(v);
  xh(:,3) = xh(:,1) - v*1E-1/norm(v) - [-v(2);v(1)]*.5E-1/norm(v);
  xh(:,4) = xr + v*2E-1;
  patch(xh(1,:),xh(2,:),max(colTmp-0.1,0),'linestyle','none');
  %Plot paddle
  tp = linspace(0,alpha,30);
  plot(xp0(1,end)+cos(tp)*0.08, xp0(2,end)-sin(tp)*0.08,'-','color',max(colTmp-0.1,0));
  plot([xp0(1) xp0(1)+.2],[xp0(2) xp0(2)],'-','color',max(colTmp-0.1,0),'linewidth',1);
  plot(xp0(1),xp0(2),'.','markersize',12,'color',max(colTmp-0.1,0));
  plot(xr(1),xr(2),'.','markersize',12,'color',max(colTmp-0.1,0));
  plot([xp0(1) xr(1)],[xp0(2) xr(2)],'-','color',max(colTmp-0.1,0),'linewidth',1.5);
  %Plot target
  plot(xg(1),xg(2),'+','markersize',12,'linewidth',4,'color',[0 0 0]);

%   %Closest point to the target
%   plot(r(1),r(2),'.','color',[0 .4 0]);
%   plot([r(1) xg(1)],[r(2) xg(2)],'-','color',[0 .4 0],'linewidth',.5);
  
%   %Various text annotations
%   text(0,0,'$\mathbf{x}_p \mathbf{x}_c \mathbf{x}_r \dot{\mathbf{x}}_r \mathbf{x}_t \mathbf{x}_e$','interpreter','latex','fontsize',16); 
%   text(xp0(1),xp0(2),'$\alpha,\dot{\alpha} r_p r_b r_c$','interpreter','latex','fontsize',16); 
  
  axis([-2.3 2.3 -2.3 2.3]); axis square;
  %axis equal;
  set(gca,'xtick',[],'ytick',[]);
  xlabel('$x_1$','interpreter','latex','fontsize',25); 
  ylabel('$x_2$','interpreter','latex','fontsize',25);
end

end

function resultsRL = demoDSGMRLearnDIPOLE(needsData,needsPlot)
%%Implements RL Learning of a DSDMP
%
%%Parameters
%RL
nbEpisods = 50;
nbInitialPoints = 30;
nbStates = 3;
%Dynamical system
sys.kP = 200;
sys.kV = 100;
sys.dt = 0.01;
sys.alpha = 0.1;
pars.sys = sys;	%Pass to the reward function
%%Task Data
%
pars.InitialPoint = [0;0];
%Viapoints
%Last viapoint is the target
%pars.Target = [0.1 0.3 0.3 0.7 1 ; 1  3  3 5 6; 0 2 -2 0 0];
pars.Target = [0.2 0.2 0.4 0.6 0.6 1 ; 1  1 2  3 3  6; 1 -1 0 0.5 -0.5 0];

P0 = pars.InitialPoint;
P1 = pars.Target([2:end],end);
nbVar = size(P0,1);

Springs = genSprings(P0,P1,nbStates,sys);
pars.Sigma = Springs.Sigma;
if needsData == 1
%%Initialize
%%%Initialize with line
%%%Iniitialize with demonstrations
%%Learn 
%Set exploration Noise

Sigma0Blk(:,:) = eye(nbVar+1);
Sigma0Blk(1,1) = Springs.Sigma(1,1,1);

%set Initial value
Sigma0 = [];
Mu0 = zeros((nbVar+1)*nbStates,1);
for k=1:nbStates
	Mu0([3*k-2:3*k],:) = Springs.Mu(:,k);
	Sigma0 = blkdiag(Sigma0,Sigma0Blk);
end
%resultsRL = rldp(Mu0,Sigma0,nbEpisods,nbInitialPoints,'viapointsRewardTime',3,1,0,0,pars);
resultsRL = rldpVec(Mu0,Sigma0,nbEpisods,nbInitialPoints,'viapointsVecReward',1,1,0,0,pars);
resultsRL(end).pars = pars;
%
save('data\resultsDIPOLE.mat','resultsRL');
end
load('data\resultsDIPOLE.mat','resultsRL');

if needsPlot == 1
%Plot
for k = 1:10*nbEpisods
	RRTmp = resultsRL(k).Mu(end-4:end,:);
	RR(k,:) = max(mean(RRTmp,1));
end
BestRR = max(RR');
AvgRR = mean(RR,2);
plot([1:10*nbEpisods],BestRR);hold on
plot([1:10*nbEpisods],AvgRR,'color','red');
figure
cmap = colormap(lines);
for k=1:size(resultsRL(end).Mu,2)
	Mu = reshape(resultsRL(end).Mu([1:end-4],k),nbVar+1,nbStates);
curve = dmpSprings(Mu,pars.Sigma,P0,pars.sys);
plot(curve.Data(1,:),curve.Data(2,:),'LineWidth',2);hold on
plot(pars.Target(2,:),pars.Target(3,:),'o','MarkerSize',20,'LineWidth',2,'color','red');
end
%curve = dmpSpringsT(Mu,pars.Sigma,P0,pars.Target(:,end),pars.sys);
%plot(curve.currTar(1,:),curve.currTar(2,:),'color','green');

end
end



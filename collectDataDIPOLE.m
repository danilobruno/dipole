function collectData()

needsData = 0;
if needsData == 1
	Tot = 10; 

	for i=1:Tot
		i
		resOld(i,:) = demoDSGMRLearnViaOld(1,0);
		res(i,:) = demoDSGMRViaVec(1,0);

	end
	save('data\collectDIPOLEVia.mat','res','resOld');
end
load('data\collectDIPOLEVia.mat','res','resOld');
sys = res(end,end).pars.sys;
pars = res(end,end).pars;
Tot = size(res,1);
TotOld = size(resOld,1);
figure()
Target = res(end,end).pars.Target;
pars.Target
plot(pars.Target(2,:),pars.Target(3,:),'o','MarkerSize',20,'LineWidth',2,'color','green');hold on
box on
grid on
nbVar = size(res(1,end).pars.Sigma,1);
nbStates = size(res(1,end).pars.Sigma,3);
nbVarOld = size(resOld(1,end).pars.Sigma,1);
nbStatesOld = size(resOld(1,end).pars.Sigma,3);
for i=1:Tot
	nbPol = size(res(i,end).Mu,2);
	for k=1:nbPol
		Mu = reshape(res(i,end).Mu([1:end-4],k),nbVar,nbStates);
		Sigma = res(i,end).pars.Sigma;
		rr = dmpSprings(Mu,Sigma,[0;0],sys);
		plot(rr.Data(1,:),rr.Data(2,:),'LineWidth',2,'color','blue');hold on
		%plot(rr.Data(1,:),rr.Data(2,:),'color','red');hold on
	end
	nbPol = size(resOld(i,end).Mu,2);
	for k=1:nbPol
		Mu = reshape(resOld(i,end).Mu([1:end-1],k),nbVarOld,nbStatesOld);
		Sigma = resOld(i,end).pars.Sigma;
		rr = dmpSprings(Mu,Sigma,[0;0],sys);
		plot(rr.Data(1,:),rr.Data(2,:),'LineWidth',2,'color','red');hold on
		%plot(rr.Data(1,:),rr.Data(2,:),'color','red');hold on
	end
end
%R=zeros(Tot,size(res,2));
for i=1:Tot
	for episode = 1:size(res,2)
		RRTmp = res(i,episode).Mu(end-4:end,:);
		Rmax(i,episode,:) = max(mean(RRTmp,1));
		Ravg(i,episode,:) = mean(mean(RRTmp,1));
	end
	for episode = 1:size(resOld,2)
		RmaxOld(i,episode,:) = max(resOld(i,episode).Mu(end,:));
		RavgOld(i,episode,:) = mean(resOld(i,episode).Mu(end,:));
	end
end
meanmaxR = mean(Rmax,1);
meanavgR = mean(Ravg,1);
varmaxR = var(Rmax,1);
varavgR = var(Ravg,1);
tottrials = [1:size(res,2)];

meanmaxROld = mean(RmaxOld,1);
meanavgROld = mean(RavgOld,1);
varmaxROld = var(RmaxOld,1);
varavgROld = var(RavgOld,1);
tottrialsOld = [1:size(resOld,2)];

figure
ymin = meanmaxR - varmaxR;
ymax = meanmaxR + varmaxR;
patch([tottrials fliplr(tottrials)],[ymin fliplr(ymax)],[0.7 1 0.7 ],'EdgeColor','none');hold on
%plot([1:length(totrPow)],totrPow,'color',[0 0 0.4]);
plot(tottrials,meanmaxR,'color',[0 0.4 0]);

ymin = meanmaxROld - varmaxROld;
ymax = meanmaxROld + varmaxROld;
patch([tottrialsOld fliplr(tottrialsOld)],[ymin fliplr(ymax)],[1 0.7 0.7 ],'EdgeColor','none');hold on
%plot([1:length(totrPow)],totrPow,'color',[0 0 0.4]);
plot(tottrialsOld,meanmaxROld,'color',[0.4 0 0]);


figure
ymin = meanavgR - varavgR;
ymax = meanavgR + varavgR;
patch([tottrials fliplr(tottrials)],[ymin fliplr(ymax)],[0.7 1 0.7 ],'EdgeColor','none');hold on
%plot([1:length(totrPow)],totrPow,'color',[0 0 0.4]);
plot(tottrials,meanavgR,'color',[0 0.4 0]);

ymin = meanavgROld - varavgROld;
ymax = meanavgROld + varavgROld;
patch([tottrialsOld fliplr(tottrialsOld)],[ymin fliplr(ymax)],[1 0.7 0.7 ],'EdgeColor','none');hold on
%plot([1:length(totrPow)],totrPow,'color',[0 0 0.4]);
plot(tottrialsOld,meanavgROld,'color',[0.4 0 0]);
end
